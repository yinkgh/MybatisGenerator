#MybatisGenerator项目逆向生成带有数据库中文注释的dao，model，xml文件。
具体用法如下：
1.修改配置文件generatorConfig.xml
修改如下：
<?xml version="1.0" encoding="UTF-8"?>
<generatorConfiguration>
	<classPathEntry location="mysql-connector-java-5.1.9.jar" />
	<context id="context1">
		<property name="javaFileEncoding" value="utf-8" />
		<property name="" value="" />
		<!-- pagination plugin -->
		<plugin type="org.mybatis.generator.plugins.SerializablePlugin"></plugin>
		<plugin type="org.mybatis.generator.plugin.PaginationMysqlPlugin" />
		<plugin type="org.mybatis.generator.plugin.CommentPlugin" />

		<commentGenerator>
			<!-- 不生成注解信息 -->
			<property name="suppressAllComments" value="true" />
		</commentGenerator>

		<jdbcConnection driverClass="org.postgresql.Driver"
			connectionURL="jdbc:postgresql://10.168.xx.xx:5432/crm_cms" userId="crm"
			password="crm" />

		<javaTypeResolver>
			<property name="forceBigDecimals" value="false" />
		</javaTypeResolver>

		<javaModelGenerator targetPackage="com.lashou.cms.domain"
			targetProject="service-cms" />
		<sqlMapGenerator targetPackage="com.lashou.cms.mapper"
			targetProject="service-cms" />
		<javaClientGenerator targetPackage="com.lashou.cms.mapper"
			targetProject="service-cms" type="XMLMAPPER" />
			
		<!-- 修改对应的数据库表 -->
		<table schema="" tableName="comment_deal" domainObjectName="CommentDeal" />

	</context>
</generatorConfiguration>


2.运行代码：

运行如下main方法即可生成对应的实体类，xml映射文件，以及dao层代码。


package org.mybatis.generator;


import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;

public class GeneratorSqlmap {

	public void generator() throws Exception{

		List<String> warnings = new ArrayList<String>();
		boolean overwrite = true;
		//指定 逆向工程配置文件
		File configFile = new File("generatorConfig.xml"); 
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(configFile);
		DefaultShellCallback callback = new DefaultShellCallback(overwrite);
		MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config,
				callback, warnings);
		myBatisGenerator.generate(null);

	} 
	public static void main(String[] args) throws Exception {
		try {
			GeneratorSqlmap generatorSqlmap = new GeneratorSqlmap();
			generatorSqlmap.generator();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
